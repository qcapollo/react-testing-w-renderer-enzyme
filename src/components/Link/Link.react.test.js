import React from "react";
import Link from "./Link.react";
import renderer from "react-test-renderer";

test("Link changes the class when hovered", () => {
  const component = renderer.create(
    <Link page="https://www.facebook.com">Facebook</Link>
  );

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  //manually trigger the callback
  tree.props.onMouseEnter();
  //re-rendering
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  //manually trigger the callback
  tree.props.onMouseLeave();
  //re-rendering
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  jest.mock("./Link.react.js", () => () => "Link");
  jest.mock("./Link.react.js", () => () => <mock-link />);
});
